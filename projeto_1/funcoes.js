const path = require('path')
const fs = require('fs')

function lerDiretorio(caminho) {
    return new Promise((resolve, reject) => {
        try {
            const arquivos = fs.readdirSync(caminho)
            const arquivosCompletos = arquivos.map(arq => path.join(caminho, arq))
            resolve(arquivosCompletos)
        } catch (error) {
            reject(error)
        }
    })
}

function lerArquivo(caminho){
    return new Promise((resolve, reject) => {

        try {
            const conteudo = fs.readFileSync(caminho, { encoding: 'utf-8' })
            resolve(conteudo.toString())
        } catch (error) {
            reject(error)
        }

    })
}

function lerArquivos(caminhos){
    return Promise.all(caminhos.map(caminho => lerArquivo(caminho)))
}

function elementosTerminadosCom(array, pardraoTextual){
    return array.filter(el => el.endsWith(pardraoTextual))
}

function removerElementosSeVazio(array){
    return array.filter(el => el.trim())
}

function removerElementosSeIncluir(pardraoTextual, array){
    return array.filter(el => !el.includes(pardraoTextual))
}

function removerElementosSeApenasNumero(array) {
    return array.filter(el => {
        const num = parseInt(el.trim())
        return num !== num
    })
}

function removerSimbolos(simbolos) {
    return function(array){
        return array.map(el => {
            return simbolos.reduce((acc, simbolo) => {
                return acc.split(simbolo).join('')
            }, el)
        })
    }
}

function mesclarConteudos(array){ 
    return array.join(' ')
}

function separarTextoPor(simbolo) {
    return function(texto){
        return texto.split(simbolo)
    }
}

function agruparElementos(palavras){
    return Object.values(palavras.reduce((acc, palavra) => {
        const el = palavra.toLowerCase()
        const qtde = acc[el] ? acc[el].qtde + 1 : 1
        acc[el] = { elemento: el, qtde }
        return acc
    }, {}))
}

function ordernarPorAtributoNumerico(attr, ordem = 'asc'){
    return function(array){
        const asc = (o1, o2) => o1[attr] - o2[attr]
        const desc = (o1, o2) => o2[attr] - o1[attr]
        return array.sort(ordem === 'asc' ? asc : desc)
    }
}

module.exports = {
    lerDiretorio,
    elementosTerminadosCom,
    lerArquivo,
    lerArquivos,
    removerElementosSeVazio,
    removerElementosSeIncluir,
    removerElementosSeApenasNumero,
    removerSimbolos,
    mesclarConteudos,
    separarTextoPor,
    agruparElementos,
    ordernarPorAtributoNumerico,
}
