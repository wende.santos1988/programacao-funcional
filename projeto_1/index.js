const path = require('path')
const fns = require('./funcoes')

const caminho = path.join(__dirname, '..', 'dados', 'legendas')

const simbolos = [
    '.', '?', '-', ',', '"', '♪',
    '_', '<i>', '</i>', '\r', '[', ']',
    '(', ')'
]




fns.lerDiretorio(caminho)
    .then(arquivos => fns.elementosTerminadosCom(arquivos, '.srt'))
    .then(arquivosSTR => fns.lerArquivos(arquivosSTR))
    .then(fns.mesclarConteudos)
    .then(fns.separarTextoPor('\n'))
    .then(fns.removerElementosSeVazio)
    .then(linhas => fns.removerElementosSeIncluir('-->', linhas))
    .then(fns.removerElementosSeApenasNumero)
    .then(fns.removerSimbolos(simbolos))
    .then(fns.mesclarConteudos)
    .then(fns.separarTextoPor(' '))
    .then(fns.removerElementosSeVazio)
    .then(fns.removerElementosSeApenasNumero)
    .then(fns.agruparElementos)
    .then(fns.ordernarPorAtributoNumerico('qtde', 'desc'))
    .then(console.log);
